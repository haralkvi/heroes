package items;

import static items.Item.Slot.WEAPON;

public class Weapon extends Item {

    private int weaponDamage;
    private WeaponType type;

    public Weapon(String name, WeaponType type, int requiredLevel, int damage) {
        super(name, requiredLevel, WEAPON);
        this.type = type;
        weaponDamage = damage;
    }

    public enum WeaponType {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    @Override
    public int sumTotalAttributes() {
        return weaponDamage;
    }

    public WeaponType getType() {
        return type;
    }
}
