/**
 * Item represents items to be equipped by Hero objects in a hypothetical RPG.
 *
 * Items require a Hero to be of a certain level to be equipped, as represented by the requiredLevel field. Each Item
 * has a slot in which it is to be applied.
 */

package items;

public abstract class Item {

    private String name;
    private int requiredLevel;
    private Slot slot;

    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    public enum Slot {
        WEAPON,
        HEAD,
        BODY,
        LEGS

    }

    public abstract int sumTotalAttributes();

    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }
}
