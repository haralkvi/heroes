package items;

import util.HeroAttribute;

public class Armor extends Item {

    private HeroAttribute armorAttributes;
    private ArmorType type;

    public Armor(String name, int requiredLevel, Slot slot, HeroAttribute armorAttributes, ArmorType type) {
        super(name, requiredLevel, slot);
        this.armorAttributes = armorAttributes;
        this.type = type;
    }

    public enum ArmorType {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE;
    }

    @Override
    public int sumTotalAttributes() {
        int armorStrength = armorAttributes.getStrength();
        int armorDexterity = armorAttributes.getDexterity();
        int armorIntelligence = armorAttributes.getIntelligence();

        return armorStrength + armorDexterity + armorIntelligence;
    }

    public ArmorType getType() {
        return type;
    }
}
