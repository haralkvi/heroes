package heroes;

import items.Armor;
import items.Weapon;
import util.HeroAttribute;

import static items.Armor.ArmorType.LEATHER;
import static items.Armor.ArmorType.MAIL;
import static items.Weapon.WeaponType.DAGGER;
import static items.Weapon.WeaponType.SWORD;

public class Rogue extends Hero {

    public Rogue(String name) {
        super(
                name,
                new HeroAttribute(2, 6, 1),
                new Weapon.WeaponType[]{DAGGER, SWORD},
                new Armor.ArmorType[]{LEATHER, MAIL});
    }

    @Override
    protected void increaseAttributes() {
        super.getLevelAttributes().increaseStrength(1);
        super.getLevelAttributes().increaseDexterity(4);
        super.getLevelAttributes().increaseIntelligence(1);
    }

    @Override
    protected int heroSpecificDamage() {
        return this.dexterity();
    }

}
