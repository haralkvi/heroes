/**
 * Hero class represents a hero in a hypothetical RPG.
 *
 * Every hero has a set of three attributes, slots for weapons as well as armor. This class contains methods for
 * manipulating and calculating total attributes, calculating damage, equipping weapon and armor.
 */

package heroes;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Item;
import items.Weapon;
import util.HeroAttribute;

import java.util.HashMap;

import static items.Item.Slot.*;

public abstract class Hero {

    private final String name;
    private int level;
    private HeroAttribute levelAttributes;
    private HashMap<Item.Slot, Item> equipment;
    private Weapon.WeaponType[] validWeaponTypes;
    private Armor.ArmorType[] validArmorTypes;

    public Hero(
            String name,
            HeroAttribute attr,
            Weapon.WeaponType[] validWeapons,
            Armor.ArmorType[] validArmor
    ) {

        this.name = name;
        this.level = 1;
        this.initializeEquipment();
        levelAttributes = attr;
        validWeaponTypes = validWeapons;
        validArmorTypes = validArmor;
    }

    private void initializeEquipment() {
        equipment = new HashMap<>();
        equipment.put(WEAPON, null);
        equipment.put(HEAD, null);
        equipment.put(BODY, null);
        equipment.put(LEGS, null);
    }

    public void levelUp() {
        level++;
        this.increaseAttributes();
    }

    protected abstract void increaseAttributes();

    public void equip(Weapon weapon) throws InvalidWeaponException {

        // In order for a hero to equip a weapon, the hero has to be of a certain level and of a certain type
        if (weapon.getRequiredLevel() > this.getLevel() ||
                !this.canEquipWeapon(weapon)) {
            throw new InvalidWeaponException("Cannot equip weapon " + weapon.getName() + "!");
        }

        equipment.put(WEAPON, weapon);
    }

    private boolean canEquipWeapon(Weapon weaponToBeEquipped) {
        boolean isEquippable = false;

        // Checks to see if a weapon is of a type that is valid for this hero.
        for (Weapon.WeaponType weaponType : validWeaponTypes) {
            if (weaponType.equals(weaponToBeEquipped.getType())) {
                isEquippable = true;
                break;
            }
        }

        return isEquippable;
    }

    public void equip(Armor armor) throws InvalidArmorException {

        // In order for a hero to equip a piece of armor, the hero has to be of a certain level and of a certain type
        if (armor.getRequiredLevel() > this.getLevel() ||
                !this.canEquipArmor(armor)) {
            throw new InvalidArmorException("Cannot equip armor " + armor.getName() + "!");
        }

        equipment.put(armor.getSlot(), armor);
    }

    private boolean canEquipArmor(Armor armorToBeEquipped) {
        boolean isEquippable = false;

        // Checks to see if a piece of armor is of a type that is valid for this hero.
        for (Armor.ArmorType armorType : validArmorTypes) {
            if (armorType.equals(armorToBeEquipped.getType())) {
                isEquippable = true;
                break;
            }
        }

        return isEquippable;
    }

    public int damage() {
        Item equippedWeapon = this.getWeapon();

        int weaponDamage = (equippedWeapon != null) ? equippedWeapon.sumTotalAttributes() : 1;
        int heroDamage = this.heroSpecificDamage();

        return weaponDamage * (1 + heroDamage / 100);
    }

    protected abstract int heroSpecificDamage();

    public int totalAttributes() {
        int sumOfLevelAttributes = this.sumOfLevelAttributes();
        int sumOfArmorAttributes = this.sumOfArmorAttributes();

        return sumOfLevelAttributes + sumOfArmorAttributes;
    }

    private int sumOfLevelAttributes() {
        int strength = this.strength();
        int dexterity = this.dexterity();
        int intelligence = this.intelligence();

        return strength + dexterity + intelligence;
    }

    private int sumOfArmorAttributes() {
        int sum = 0;

        for (Item.Slot slot : equipment.keySet()) {
            if ((equipment.get(slot) != null) && (!slot.equals(WEAPON))) {
                Item pieceOfArmor = equipment.get(slot);
                sum += pieceOfArmor.sumTotalAttributes();
            }
        }

        return sum;
    }

    public int strength() {
        return levelAttributes.getStrength();
    }

    public int dexterity() {
        return levelAttributes.getDexterity();
    }

    public int intelligence() {
        return levelAttributes.getIntelligence();
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HeroAttribute getLevelAttributes() {
        return levelAttributes;
    }

    protected Item getWeapon() {
        return equipment.get(WEAPON);
    }

    public String display() {

        StringBuilder displayString = new StringBuilder();

        displayString.append("* Name: " + this.name + "\n");
        displayString.append("* Class: " + this.getClass().getName() + "\n");
        displayString.append("* Level: " + this.level + "\n");
        displayString.append("* Strength: " + this.strength() + "\n");
        displayString.append("* Dexterity: " + this.dexterity() + "\n");
        displayString.append("* Intelligence: " + this.intelligence() + "\n");
        displayString.append("* Damage: " + this.damage() + "\n");

        return displayString.toString();
    }
}
