package heroes;

import items.Armor;
import items.Weapon;
import util.HeroAttribute;

import static items.Armor.ArmorType.CLOTH;
import static items.Weapon.WeaponType.STAFF;
import static items.Weapon.WeaponType.WAND;

public class Mage extends Hero {

    public Mage(String name) {
        super(name,
                new HeroAttribute(1, 1, 8),
                new Weapon.WeaponType[]{STAFF, WAND},
                new Armor.ArmorType[]{CLOTH}
        );
    }

    @Override
    protected void increaseAttributes() {
        super.getLevelAttributes().increaseStrength(1);
        super.getLevelAttributes().increaseDexterity(1);
        super.getLevelAttributes().increaseIntelligence(5);
    }

    @Override
    protected int heroSpecificDamage() {
        return this.intelligence();
    }
}
