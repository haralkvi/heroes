package heroes;

import items.Armor;
import items.Weapon;
import util.HeroAttribute;

import static items.Armor.ArmorType.MAIL;
import static items.Armor.ArmorType.PLATE;
import static items.Weapon.WeaponType.*;

public class Warrior extends Hero {
    public Warrior(String name) {
        super(
                name,
                new HeroAttribute(5, 2, 1),
                new Weapon.WeaponType[]{AXE, HAMMER, SWORD},
                new Armor.ArmorType[]{MAIL, PLATE});
    }

    @Override
    protected void increaseAttributes() {
        super.getLevelAttributes().increaseStrength(3);
        super.getLevelAttributes().increaseDexterity(2);
        super.getLevelAttributes().increaseIntelligence(1);
    }

    @Override
    protected int heroSpecificDamage() {
        return this.strength();
    }
}
