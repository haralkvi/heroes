package heroes;

import items.Armor;
import items.Weapon;
import util.HeroAttribute;

import static items.Armor.ArmorType.LEATHER;
import static items.Armor.ArmorType.MAIL;
import static items.Weapon.WeaponType.BOW;

public class Ranger extends Hero {

    public Ranger(String name) {
        super(name,
                new HeroAttribute(1, 7, 1),
                new Weapon.WeaponType[]{BOW},
                new Armor.ArmorType[]{LEATHER, MAIL});
    }

    @Override
    protected void increaseAttributes() {
        super.getLevelAttributes().increaseStrength(1);
        super.getLevelAttributes().increaseDexterity(5);
        super.getLevelAttributes().increaseIntelligence(1);
    }

    @Override
    protected int heroSpecificDamage() {
        return this.dexterity();
    }

}
