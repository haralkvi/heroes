/**
 * HeroAttribute is basically a data structure for storing, accessing, manipulating and displaying the game's three main
 * stats: strength, dexterity and intelligence.
 */

package util;

public class HeroAttribute {

    private int strength;
    private int dexterity;
    private int intelligence;

    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public void increaseStrength(int amount) {
        strength += amount;
    }

    public void increaseDexterity(int amount) {
        dexterity += amount;
    }

    public void increaseIntelligence(int amount) {
        intelligence += amount;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public String toString() {
        return "{Strength: " + strength +
                ", dexterity: " + dexterity +
                ", intelligence: " + intelligence;
    }
}
