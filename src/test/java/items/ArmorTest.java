package items;

import org.junit.jupiter.api.Test;
import util.HeroAttribute;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArmorTest {

    @Test
    public void getName_armor_shouldReturnCorrectName() {
        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.CLOTH
        );

        String expected = "Name";

        String actual = armor.getName();

        assertEquals(expected, actual);
    }

    @Test
    public void getRequiredLevel_armor_shouldReturnCorrectRequiredLevel() {
        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.CLOTH
        );

        int expected = 1;

        int actual = armor.getRequiredLevel();

        assertEquals(expected, actual);
    }

    @Test
    public void getArmorType_armor_shouldReturnCorrectArmorType() {
        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.CLOTH
        );

        Armor.ArmorType expected = Armor.ArmorType.CLOTH;

        Armor.ArmorType actual = armor.getType();

        assertEquals(expected, actual);
    }

    // TODO: Split into three tests
    @Test
    public void getArmorDamage_armor_shouldReturnCorrectAttributes() {
        int armorStrength = 1;
        int armorDexterity = 1;
        int armorIntelligence = 1;

        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(armorStrength, armorDexterity, armorIntelligence),
                Armor.ArmorType.CLOTH
        );

        int expected = armorStrength + armorDexterity + armorIntelligence;

        int actual = armor.sumTotalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void getArmorSlot_armor_shouldReturnCorrectSlot() {
        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.CLOTH
        );
        Item.Slot expected = Item.Slot.BODY;

        Item.Slot actual = armor.getSlot();

        assertEquals(expected, actual);
    }

}