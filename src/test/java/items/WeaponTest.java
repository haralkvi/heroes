package items;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WeaponTest {

    @Test
    public void getName_weapon_shouldReturnCorrectName() {
        Weapon weapon = new Weapon("Name", Weapon.WeaponType.AXE, 1, 1);
        String expected = "Name";

        String actual = weapon.getName();

        assertEquals(expected, actual);
    }

    @Test
    public void getRequiredLevel_weapon_shouldReturnCorrectRequiredLevel() {
        Weapon weapon = new Weapon("Name", Weapon.WeaponType.AXE, 1, 1);
        int expected = 1;

        int actual = weapon.getRequiredLevel();

        assertEquals(expected, actual);
    }

    @Test
    public void getWeaponType_weapon_shouldReturnCorrectWeaponType() {
        Weapon weapon = new Weapon("Name", Weapon.WeaponType.AXE, 1, 1);
        Weapon.WeaponType expected = Weapon.WeaponType.AXE;

        Weapon.WeaponType actual = weapon.getType();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_weapon_shouldReturnCorrectDamage() {
        Weapon weapon = new Weapon("Name", Weapon.WeaponType.AXE, 1, 1);
        int expected = 1;

        int actual = weapon.sumTotalAttributes();

        assertEquals(expected, actual);
    }

}