package items;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemTest {

    @Test
    public void getName_item_shouldReturnCorrectName() {
        Item item = new Weapon("Name", Weapon.WeaponType.AXE, 1, 1);
        String expected = "Name";

        String actual = item.getName();

        assertEquals(expected, actual);
    }

    @Test
    public void getRequiredLevel_item_shouldReturnCorrectRequiredLevel() {
        Item item = new Weapon("Name", Weapon.WeaponType.AXE, 1, 1);
        int expected = 1;

        int actual = item.getRequiredLevel();

        assertEquals(expected, actual);
    }

}