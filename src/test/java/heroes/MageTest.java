package heroes;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Item;
import items.Weapon;
import org.junit.jupiter.api.Test;
import util.HeroAttribute;

import static items.Weapon.WeaponType.STAFF;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MageTest {

    @Test
    public void strength_initializedMage_shouldReturnOne() {
        Mage mage = new Mage("Gandalf");
        int expected = 1;
        int actual = mage.strength();
        assertEquals(expected, actual);
    }

    @Test
    void dexterity_initializedMage_shouldReturnOne() {
        Mage mage = new Mage("Gandalf");
        int expected = 1;
        int actual = mage.dexterity();
        assertEquals(expected, actual);
    }

    @Test
    void intelligence_initializedMage_shouldReturnEight() {
        Mage mage = new Mage("Gandalf");
        int expected = 8;
        int actual = mage.intelligence();
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedMage_shouldIncreaseLevelByOne() {
        Mage mage = new Mage("Gandalf");
        int initialLevel = 1;
        int levelIncrease = 1;
        int expected = initialLevel + levelIncrease;

        mage.levelUp();
        int actual = mage.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedMage_shouldIncreaseStrengthByOne() {
        Mage mage = new Mage("Gandalf");
        int initialStrength = mage.strength();
        int strengthIncrease = 1;
        int expected = initialStrength + strengthIncrease;

        mage.levelUp();
        int actual = mage.strength();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedMage_shouldIncreaseDexterityByOne() {
        Mage mage = new Mage("Gandalf");
        int initialDexterity = mage.dexterity();
        int dexterityIncrease = 1;
        int expected = initialDexterity + dexterityIncrease;

        mage.levelUp();
        int actual = mage.dexterity();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedMage_shouldIncreaseIntelligenceByFive() {
        Mage mage = new Mage("Gandalf");
        int initialIntelligence = mage.intelligence();
        int intelligenceIncrease = 5;
        int expected = initialIntelligence + intelligenceIncrease;

        mage.levelUp();
        int actual = mage.intelligence();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_noArmorEquipped_shouldReturnCorrectSum() {
        Mage mage = new Mage("Gandalf");
        int heroStrength = 1;
        int heroDexterity = 1;
        int heroIntelligence = 8;

        // The following three variables are declared and initialized in order
        // to make calculations as explicit as possible
        int armorStrength = 0;
        int armorDexterity = 0;
        int armorIntelligence = 0;

        int sumOfMageAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfArmorAttributes = armorStrength + armorDexterity + armorIntelligence;

        int expected = sumOfMageAttributes + sumOfArmorAttributes;

        int actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_onePieceOfArmorEquipped_shouldReturnCorrectSum() throws InvalidArmorException {
        Mage mage = new Mage("Gandalf");
        int heroStrength = 1;
        int heroDexterity = 1;
        int heroIntelligence = 8;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int sumOfMageAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.CLOTH
        );

        int expected = sumOfMageAttributes + sumOfArmorAttributes;

        mage.equip(bodyArmor);
        int actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_twoPiecesOfEquipment_shouldReturnCorrectSum() throws InvalidArmorException {
        Mage mage = new Mage("Gandalf");
        int heroStrength = 1;
        int heroDexterity = 1;
        int heroIntelligence = 8;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int headArmorStrength = 1;
        int headArmorDexterity = 1;
        int headArmorIntelligence = 1;

        int sumOfMageAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;
        int sumOfHeadArmorAttributes = headArmorStrength + headArmorDexterity + headArmorIntelligence;

        int sumOfAllArmorAttributes = sumOfBodyArmorAttributes + sumOfHeadArmorAttributes;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.CLOTH
        );

        Armor headArmor = new Armor(
                "Name",
                1,
                Item.Slot.HEAD,
                new HeroAttribute(headArmorStrength, headArmorDexterity, headArmorIntelligence),
                Armor.ArmorType.CLOTH
        );

        int expected = sumOfMageAttributes + sumOfAllArmorAttributes;

        mage.equip(bodyArmor);
        mage.equip(headArmor);
        int actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_threePiecesOfEquipment_shouldReturnCorrectSum() throws InvalidArmorException {
        Mage mage = new Mage("Gandalf");
        int heroStrength = 1;
        int heroDexterity = 1;
        int heroIntelligence = 8;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int headArmorStrength = 1;
        int headArmorDexterity = 1;
        int headArmorIntelligence = 1;

        int legsArmorStrength = 1;
        int legsArmorDexterity = 1;
        int legsArmorIntelligence = 1;

        int sumOfMageAttributes = heroStrength + heroDexterity + heroIntelligence;

        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;
        int sumOfHeadArmorAttributes = headArmorStrength + headArmorDexterity + headArmorIntelligence;
        int sumOfLegsArmorAttributes = legsArmorStrength + legsArmorDexterity + legsArmorIntelligence;

        int sumOfAllArmorAttributes = sumOfBodyArmorAttributes + sumOfHeadArmorAttributes + sumOfLegsArmorAttributes;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.CLOTH
        );

        Armor headArmor = new Armor(
                "Name",
                1,
                Item.Slot.HEAD,
                new HeroAttribute(headArmorStrength, headArmorDexterity, headArmorIntelligence),
                Armor.ArmorType.CLOTH
        );

        Armor legsArmor = new Armor(
                "Name",
                1,
                Item.Slot.LEGS,
                new HeroAttribute(legsArmorStrength, legsArmorDexterity, legsArmorIntelligence),
                Armor.ArmorType.CLOTH
        );

        int expected = sumOfMageAttributes + sumOfAllArmorAttributes;

        mage.equip(bodyArmor);
        mage.equip(headArmor);
        mage.equip(legsArmor);
        int actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_equipmentInGivenSpotReplaced_shouldReturnCorrectSum() throws InvalidArmorException {
        Mage mage = new Mage("Gandalf");
        int heroStrength = 1;
        int heroDexterity = 1;
        int heroIntelligence = 8;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int altBodyArmorStrength = 1;
        int altBodyArmorDexterity = 1;
        int altBodyArmorIntelligence = 1;

        int sumOfMageAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.CLOTH
        );

        Armor altArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(altBodyArmorStrength, altBodyArmorDexterity, altBodyArmorIntelligence),
                Armor.ArmorType.CLOTH
        );

        int expected = sumOfMageAttributes + sumOfBodyArmorAttributes;

        mage.equip(bodyArmor);
        mage.equip(altArmor);
        int actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_noWeaponEquipped_shouldReturnCorrectSum() {
        Mage mage = new Mage("Gandalf");
        int damagingAttributes = mage.intelligence(); // A mage's damaging attribute is intelligence.
        int weaponDamage = 1; // Weapon damage is set to 1 when there is no weapon.
        int expected = weaponDamage * (1 + damagingAttributes / 100);

        int actual = mage.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponEquipped_shouldReturnCorrectSum() throws InvalidWeaponException {
        Mage mage = new Mage("Gandalf");
        int damagingAttributes = mage.intelligence(); // A mage's damaging attribute is intelligence.

        Weapon weapon = new Weapon("Name", STAFF, 1, 2);
        int weaponDamage = weapon.sumTotalAttributes();
        int expected = weaponDamage * (1 + damagingAttributes / 100);

        mage.equip(weapon);
        int actual = mage.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponEquippedAndReplaced_shouldReturnCorrectSum() throws InvalidWeaponException {
        Mage mage = new Mage("Gandalf");
        int damagingAttributes = mage.intelligence(); // A mage's damaging attribute is intelligence.

        Weapon weapon = new Weapon("Name", STAFF, 1, 2);
        Weapon altWeapon = new Weapon("Name", STAFF, 1, 2);

        int altWeaponDamage = altWeapon.sumTotalAttributes();
        int expected = altWeaponDamage * (1 + damagingAttributes / 100);

        mage.equip(weapon);
        mage.equip(altWeapon);
        int actual = mage.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponAndArmorEquipped_shouldReturnCorrectSum() throws InvalidWeaponException, InvalidArmorException {
        Mage mage = new Mage("Gandalf");
        int damagingAttributes = mage.intelligence(); // A mage's damaging attribute is intelligence.

        Weapon weapon = new Weapon("Name", STAFF, 1, 2);
        int weaponDamage = weapon.sumTotalAttributes();

        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.CLOTH
        );

        int expected = weaponDamage * (1 + damagingAttributes / 100);

        mage.equip(armor);
        mage.equip(weapon);
        int actual = mage.damage();

        assertEquals(expected, actual);
    }
}