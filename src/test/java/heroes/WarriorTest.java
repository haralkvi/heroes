package heroes;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Item;
import items.Weapon;
import org.junit.jupiter.api.Test;
import util.HeroAttribute;

import static items.Weapon.WeaponType.SWORD;
import static org.junit.jupiter.api.Assertions.assertEquals;

class WarriorTest {
    @Test
    public void strength_initializedWarrior_shouldReturnFive() {
        Warrior warrior = new Warrior("Wario");
        int expected = 5;
        int actual = warrior.strength();
        assertEquals(expected, actual);
    }

    @Test
    void dexterity_initializedWarrior_shouldReturnTwo() {
        Warrior warrior = new Warrior("Wario");
        int expected = 2;
        int actual = warrior.dexterity();
        assertEquals(expected, actual);
    }

    @Test
    void intelligence_initializedWarrior_shouldReturnOne() {
        Warrior warrior = new Warrior("Wario");
        int expected = 1;
        int actual = warrior.intelligence();
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedWarrior_shouldIncreaseLevelByOne() {
        Warrior warrior = new Warrior("Wario");
        int initialLevel = 1;
        int levelIncrease = 1;
        int expected = initialLevel + levelIncrease;

        warrior.levelUp();
        int actual = warrior.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedWarrior_shouldIncreaseStrengthByThree() {
        Warrior warrior = new Warrior("Wario");
        int initialStrength = warrior.strength();
        int strengthIncrease = 3;
        int expected = initialStrength + strengthIncrease;

        warrior.levelUp();
        int actual = warrior.strength();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedWarrior_shouldIncreaseDexterityByTwo() {
        Warrior warrior = new Warrior("Wario");
        int initialDexterity = warrior.dexterity();
        int dexterityIncrease = 2;
        int expected = initialDexterity + dexterityIncrease;

        warrior.levelUp();
        int actual = warrior.dexterity();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedWarrior_shouldIncreaseIntelligenceByOne() {
        Warrior warrior = new Warrior("Wario");
        int initialIntelligence = warrior.intelligence();
        int intelligenceIncrease = 1;
        int expected = initialIntelligence + intelligenceIncrease;

        warrior.levelUp();
        int actual = warrior.intelligence();

        assertEquals(expected, actual);
    }


    @Test
    public void sumTotalAttributes_noArmorEquipped_shouldReturnCorrectSum() {
        Warrior warrior = new Warrior("Wario");
        int heroStrength = 5;
        int heroDexterity = 2;
        int heroIntelligence = 1;

        // The following three variables are declared and initialized in order
        // to make calculations as explicit as possible
        int armorStrength = 0;
        int armorDexterity = 0;
        int armorIntelligence = 0;

        int sumOfWarriorAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfArmorAttributes = armorStrength + armorDexterity + armorIntelligence;

        int expected = sumOfWarriorAttributes + sumOfArmorAttributes;

        int actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_onePieceOfArmorEquipped_shouldReturnCorrectSum() throws InvalidArmorException {
        Warrior warrior = new Warrior("Wario");
        int heroStrength = 5;
        int heroDexterity = 2;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int sumOfWarriorAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfWarriorAttributes + sumOfArmorAttributes;

        warrior.equip(bodyArmor);
        int actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_twoPiecesOfEquipment_shouldReturnCorrectSum() throws InvalidArmorException {
        Warrior warrior = new Warrior("Wario");
        int heroStrength = 5;
        int heroDexterity = 2;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int headArmorStrength = 1;
        int headArmorDexterity = 1;
        int headArmorIntelligence = 1;

        int sumOfWarriorAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;
        int sumOfHeadArmorAttributes = headArmorStrength + headArmorDexterity + headArmorIntelligence;

        int sumOfAllArmorAttributes = sumOfBodyArmorAttributes + sumOfHeadArmorAttributes;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor headArmor = new Armor(
                "Name",
                1,
                Item.Slot.HEAD,
                new HeroAttribute(headArmorStrength, headArmorDexterity, headArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfWarriorAttributes + sumOfAllArmorAttributes;

        warrior.equip(bodyArmor);
        warrior.equip(headArmor);
        int actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_threePiecesOfEquipment_shouldReturnCorrectSum() throws InvalidArmorException {
        Warrior warrior = new Warrior("Wario");
        int heroStrength = 5;
        int heroDexterity = 2;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int headArmorStrength = 1;
        int headArmorDexterity = 1;
        int headArmorIntelligence = 1;

        int legsArmorStrength = 1;
        int legsArmorDexterity = 1;
        int legsArmorIntelligence = 1;

        int sumOfWarriorAttributes = heroStrength + heroDexterity + heroIntelligence;

        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;
        int sumOfHeadArmorAttributes = headArmorStrength + headArmorDexterity + headArmorIntelligence;
        int sumOfLegsArmorAttributes = legsArmorStrength + legsArmorDexterity + legsArmorIntelligence;

        int sumOfAllArmorAttributes = sumOfBodyArmorAttributes + sumOfHeadArmorAttributes + sumOfLegsArmorAttributes;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor headArmor = new Armor(
                "Name",
                1,
                Item.Slot.HEAD,
                new HeroAttribute(headArmorStrength, headArmorDexterity, headArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor legsArmor = new Armor(
                "Name",
                1,
                Item.Slot.LEGS,
                new HeroAttribute(legsArmorStrength, legsArmorDexterity, legsArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfWarriorAttributes + sumOfAllArmorAttributes;

        warrior.equip(bodyArmor);
        warrior.equip(headArmor);
        warrior.equip(legsArmor);
        int actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_equipmentInGivenSpotReplaced_shouldReturnCorrectSum() throws InvalidArmorException {
        Warrior warrior = new Warrior("Wario");
        int heroStrength = 5;
        int heroDexterity = 2;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int altBodyArmorStrength = 1;
        int altBodyArmorDexterity = 1;
        int altBodyArmorIntelligence = 1;

        int sumOfWarriorAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor altArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(altBodyArmorStrength, altBodyArmorDexterity, altBodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfWarriorAttributes + sumOfBodyArmorAttributes;

        warrior.equip(bodyArmor);
        warrior.equip(altArmor);
        int actual = warrior.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_noWeaponEquipped_shouldReturnCorrectSum() {
        Warrior warrior = new Warrior("Wario");
        int damagingAttributes = warrior.strength(); // A warrior's damaging attribute is strength.
        int weaponDamage = 1; // Weapon damage is set to 1 when there is no weapon.
        int expected = weaponDamage * (1 + damagingAttributes / 100);

        int actual = warrior.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponEquipped_shouldReturnCorrectSum() throws InvalidWeaponException {
        Warrior warrior = new Warrior("Wario");
        int damagingAttributes = warrior.strength(); // A warrior's damaging attribute is strength.

        Weapon weapon = new Weapon("Name", SWORD, 1, 2);
        int weaponDamage = weapon.sumTotalAttributes();
        int expected = weaponDamage * (1 + damagingAttributes / 100);

        warrior.equip(weapon);
        int actual = warrior.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponEquippedAndReplaced_shouldReturnCorrectSum() throws InvalidWeaponException {
        Warrior warrior = new Warrior("Wario");
        int damagingAttributes = warrior.strength(); // A warrior's damaging attribute is strength.

        Weapon weapon = new Weapon("Name", SWORD, 1, 2);
        Weapon altWeapon = new Weapon("Name", SWORD, 1, 2);

        int altWeaponDamage = altWeapon.sumTotalAttributes();
        int expected = altWeaponDamage * (1 + damagingAttributes / 100);

        warrior.equip(weapon);
        warrior.equip(altWeapon);
        int actual = warrior.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponAndArmorEquipped_shouldReturnCorrectSum() throws InvalidWeaponException, InvalidArmorException {
        Warrior warrior = new Warrior("Wario");
        int damagingAttributes = warrior.strength(); // A warrior's damaging attribute is strength.

        Weapon weapon = new Weapon("Name", SWORD, 1, 2);
        int weaponDamage = weapon.sumTotalAttributes();

        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.MAIL
        );

        int expected = weaponDamage * (1 + damagingAttributes / 100);

        warrior.equip(armor);
        warrior.equip(weapon);
        int actual = warrior.damage();

        assertEquals(expected, actual);
    }
}