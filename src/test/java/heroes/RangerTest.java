package heroes;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Item;
import items.Weapon;
import org.junit.jupiter.api.Test;
import util.HeroAttribute;

import static items.Weapon.WeaponType.BOW;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RangerTest {

    @Test
    public void strength_initializedRanger_shouldReturnOne() {
        Ranger ranger = new Ranger("Aragorn");
        int expected = 1;
        int actual = ranger.strength();
        assertEquals(expected, actual);
    }

    @Test
    void dexterity_initializedRanger_shouldReturnSeven() {
        Ranger ranger = new Ranger("Aragorn");
        int expected = 7;
        int actual = ranger.dexterity();
        assertEquals(expected, actual);
    }

    @Test
    void intelligence_initializedRanger_shouldReturnOne() {
        Ranger ranger = new Ranger("Aragorn");
        int expected = 1;
        int actual = ranger.intelligence();
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedRanger_shouldIncreaseLevelByOne() {
        Ranger ranger = new Ranger("Aragorn");
        int initialLevel = 1;
        int levelIncrease = 1;
        int expected = initialLevel + levelIncrease;

        ranger.levelUp();
        int actual = ranger.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedRanger_shouldIncreaseStrengthByOne() {
        Ranger ranger = new Ranger("Aragorn");
        int initialStrength = ranger.strength();
        int strengthIncrease = 1;
        int expected = initialStrength + strengthIncrease;

        ranger.levelUp();
        int actual = ranger.strength();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedRanger_shouldIncreaseDexterityByFive() {
        Ranger ranger = new Ranger("Aragorn");
        int initialDexterity = ranger.dexterity();
        int dexterityIncrease = 5;
        int expected = initialDexterity + dexterityIncrease;

        ranger.levelUp();
        int actual = ranger.dexterity();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedRanger_shouldIncreaseIntelligenceByOne() {
        Ranger ranger = new Ranger("Aragorn");
        int initialIntelligence = ranger.intelligence();
        int intelligenceIncrease = 1;
        int expected = initialIntelligence + intelligenceIncrease;

        ranger.levelUp();
        int actual = ranger.intelligence();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_noArmorEquipped_shouldReturnCorrectSum() {
        Ranger ranger = new Ranger("Aragorn");
        int heroStrength = 1;
        int heroDexterity = 7;
        int heroIntelligence = 1;

        // The following three variables are declared and initialized in order
        // to make calculations as explicit as possible
        int armorStrength = 0;
        int armorDexterity = 0;
        int armorIntelligence = 0;

        int sumOfRangerAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfArmorAttributes = armorStrength + armorDexterity + armorIntelligence;

        int expected = sumOfRangerAttributes + sumOfArmorAttributes;

        int actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_onePieceOfArmorEquipped_shouldReturnCorrectSum() throws InvalidArmorException {
        Ranger ranger = new Ranger("Aragorn");
        int heroStrength = 1;
        int heroDexterity = 7;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int sumOfRangerAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfRangerAttributes + sumOfArmorAttributes;

        ranger.equip(bodyArmor);
        int actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_twoPiecesOfEquipment_shouldReturnCorrectSum() throws InvalidArmorException {
        Ranger ranger = new Ranger("Aragorn");
        int heroStrength = 1;
        int heroDexterity = 7;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int headArmorStrength = 1;
        int headArmorDexterity = 1;
        int headArmorIntelligence = 1;

        int sumOfRangerAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;
        int sumOfHeadArmorAttributes = headArmorStrength + headArmorDexterity + headArmorIntelligence;

        int sumOfAllArmorAttributes = sumOfBodyArmorAttributes + sumOfHeadArmorAttributes;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor headArmor = new Armor(
                "Name",
                1,
                Item.Slot.HEAD,
                new HeroAttribute(headArmorStrength, headArmorDexterity, headArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfRangerAttributes + sumOfAllArmorAttributes;

        ranger.equip(bodyArmor);
        ranger.equip(headArmor);
        int actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_threePiecesOfEquipment_shouldReturnCorrectSum() throws InvalidArmorException {
        Ranger ranger = new Ranger("Aragorn");
        int heroStrength = 1;
        int heroDexterity = 7;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int headArmorStrength = 1;
        int headArmorDexterity = 1;
        int headArmorIntelligence = 1;

        int legsArmorStrength = 1;
        int legsArmorDexterity = 1;
        int legsArmorIntelligence = 1;

        int sumOfRangerAttributes = heroStrength + heroDexterity + heroIntelligence;

        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;
        int sumOfHeadArmorAttributes = headArmorStrength + headArmorDexterity + headArmorIntelligence;
        int sumOfLegsArmorAttributes = legsArmorStrength + legsArmorDexterity + legsArmorIntelligence;

        int sumOfAllArmorAttributes = sumOfBodyArmorAttributes + sumOfHeadArmorAttributes + sumOfLegsArmorAttributes;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor headArmor = new Armor(
                "Name",
                1,
                Item.Slot.HEAD,
                new HeroAttribute(headArmorStrength, headArmorDexterity, headArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor legsArmor = new Armor(
                "Name",
                1,
                Item.Slot.LEGS,
                new HeroAttribute(legsArmorStrength, legsArmorDexterity, legsArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfRangerAttributes + sumOfAllArmorAttributes;

        ranger.equip(bodyArmor);
        ranger.equip(headArmor);
        ranger.equip(legsArmor);
        int actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }


    @Test
    public void sumTotalAttributes_equipmentInGivenSpotReplaced_shouldReturnCorrectSum() throws InvalidArmorException {
        Ranger ranger = new Ranger("Aragorn");
        int heroStrength = 1;
        int heroDexterity = 7;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int altBodyArmorStrength = 1;
        int altBodyArmorDexterity = 1;
        int altBodyArmorIntelligence = 1;

        int sumOfRangerAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor altArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(altBodyArmorStrength, altBodyArmorDexterity, altBodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfRangerAttributes + sumOfBodyArmorAttributes;

        ranger.equip(bodyArmor);
        ranger.equip(altArmor);
        int actual = ranger.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_noWeaponEquipped_shouldReturnCorrectSum() {
        Ranger ranger = new Ranger("Aragorn");
        int damagingAttributes = ranger.dexterity(); // A ranger's damaging attribute is dexterity.
        int weaponDamage = 1; // Weapon damage is set to 1 when there is no weapon.
        int expected = weaponDamage * (1 + damagingAttributes / 100);

        int actual = ranger.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponEquipped_shouldReturnCorrectSum() throws InvalidWeaponException {
        Ranger ranger = new Ranger("Aragorn");
        int damagingAttributes = ranger.dexterity(); // A ranger's damaging attribute is dexterity.

        Weapon weapon = new Weapon("Name", BOW, 1, 2);
        int weaponDamage = weapon.sumTotalAttributes();
        int expected = weaponDamage * (1 + damagingAttributes / 100);

        ranger.equip(weapon);
        int actual = ranger.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponEquippedAndReplaced_shouldReturnCorrectSum() throws InvalidWeaponException {
        Ranger ranger = new Ranger("Aragorn");
        int damagingAttributes = ranger.dexterity(); // A ranger's damaging attribute is dexterity.

        Weapon weapon = new Weapon("Name", BOW, 1, 2);
        Weapon altWeapon = new Weapon("Name", BOW, 1, 2);

        int altWeaponDamage = altWeapon.sumTotalAttributes();
        int expected = altWeaponDamage * (1 + damagingAttributes / 100);

        ranger.equip(weapon);
        ranger.equip(altWeapon);
        int actual = ranger.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponAndArmorEquipped_shouldReturnCorrectSum() throws InvalidWeaponException, InvalidArmorException {
        Ranger ranger = new Ranger("Aragorn");
        int damagingAttributes = ranger.dexterity(); // A ranger's damaging attribute is dexterity.

        Weapon weapon = new Weapon("Name", BOW, 1, 2);
        int weaponDamage = weapon.sumTotalAttributes();

        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.MAIL
        );

        int expected = weaponDamage * (1 + damagingAttributes / 100);

        ranger.equip(armor);
        ranger.equip(weapon);
        int actual = ranger.damage();

        assertEquals(expected, actual);
    }
}