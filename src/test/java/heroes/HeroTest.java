package heroes;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Item;
import items.Weapon;
import org.junit.jupiter.api.Test;
import util.HeroAttribute;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    public void initialized_hero_shouldBeLvlOne() {
        // arrange
        Hero mage = new Mage("Gandalf");
        int expectedLevel = 1;
        // act
        int actualLevel = mage.getLevel();
        // assert
        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void initialized_hero_shouldHaveCorrectName() {
        Hero mage = new Mage("Gandalf");
        String expectedName = "Gandalf";
        String actualName = mage.getName();
        assertTrue(expectedName.equals(actualName));
    }

    @Test
    public void equipWeapon_wrongType_shouldThrowInvalidWeaponException() {
        Hero mage = new Mage("Gandalf");
        Weapon weapon = new Weapon("Name", Weapon.WeaponType.AXE, 1, 1);

        Exception exception = assertThrows(InvalidWeaponException.class, () ->
                mage.equip(weapon));

        assertEquals("Cannot equip weapon " + weapon.getName() + "!", exception.getMessage());
    }

    @Test
    public void equipWeapon_requiredLevelIsHigherThanHeroLevel_shouldThrowInvalidWeaponException() {
        Hero mage = new Mage("Gandalf");
        Weapon weapon = new Weapon("Name", Weapon.WeaponType.STAFF, 2, 1);

        Exception exception = assertThrows(InvalidWeaponException.class, () ->
                mage.equip(weapon));

        assertEquals("Cannot equip weapon " + weapon.getName() + "!", exception.getMessage());
    }

    @Test
    public void equipArmor_wrongType_shouldThrowInvalidArmorException() {
        Hero mage = new Mage("Gandalf");
        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.MAIL
        );

        Exception exception = assertThrows(InvalidArmorException.class, () ->
                mage.equip(armor));

        assertEquals("Cannot equip armor " + armor.getName() + "!", exception.getMessage());
    }

    @Test
    public void equipArmor_requiredLevelIsHigherThanHeroLevel_shouldThrowInvalidArmorException() {
        Hero mage = new Mage("Gandalf");
        Armor armor = new Armor(
                "Name",
                2,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.CLOTH
        );

        Exception exception = assertThrows(InvalidArmorException.class, () ->
                mage.equip(armor));

        assertEquals("Cannot equip armor " + armor.getName() + "!", exception.getMessage());
    }

    @Test
    public void equipWeapon_validWeapon_shouldNotThrowException() {
        Hero mage = new Mage("Gandalf");
        Weapon weapon = new Weapon("Name", Weapon.WeaponType.STAFF, 1, 1);

        assertDoesNotThrow(() -> {
            mage.equip(weapon);
        });
    }

    @Test
    public void equipArmor_validArmor_shouldNotThrowException() {
        Hero mage = new Mage("Gandalf");
        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.CLOTH
        );

        assertDoesNotThrow(() -> {
            mage.equip(armor);
        });
    }

    @Test
    public void display_shouldRelevantInfoCorrectly() {
        Hero hero = new Mage("Gandalf");

        String name = hero.getName();
        String className = hero.getClass().getName();
        int level = hero.getLevel();
        int strength = hero.strength();
        int dexterity = hero.dexterity();
        int intelligence = hero.intelligence();
        int damage = hero.damage();

        StringBuilder displayString = new StringBuilder();
        displayString.append("* Name: " + name + "\n");
        displayString.append("* Class: " + className + "\n");
        displayString.append("* Level: " + level + "\n");
        displayString.append("* Strength: " + strength + "\n");
        displayString.append("* Dexterity: " + dexterity + "\n");
        displayString.append("* Intelligence: " + intelligence + "\n");
        displayString.append("* Damage: " + damage + "\n");

        String expected = displayString.toString();

        String actual = hero.display();

        assertEquals(expected, actual);
    }

}