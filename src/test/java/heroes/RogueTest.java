package heroes;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Item;
import items.Weapon;
import org.junit.jupiter.api.Test;
import util.HeroAttribute;

import static items.Weapon.WeaponType.SWORD;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RogueTest {

    @Test
    public void strength_initializedRogue_shouldReturnTwo() {
        Rogue rogue = new Rogue("Hades");
        int expected = 2;
        int actual = rogue.strength();
        assertEquals(expected, actual);
    }

    @Test
    void dexterity_initializedRogue_shouldReturnSix() {
        Rogue rogue = new Rogue("Hades");
        int expected = 6;
        int actual = rogue.dexterity();
        assertEquals(expected, actual);
    }

    @Test
    void intelligence_initializedRogue_shouldReturnOne() {
        Rogue rogue = new Rogue("Hades");
        int expected = 1;
        int actual = rogue.intelligence();
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedRogue_shouldIncreaseLevelByOne() {
        Rogue rogue = new Rogue("Hades");
        int initialLevel = 1;
        int levelIncrease = 1;
        int expected = initialLevel + levelIncrease;

        rogue.levelUp();
        int actual = rogue.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedRogue_shouldIncreaseStrengthByOne() {
        Rogue rogue = new Rogue("Hades");
        int initialStrength = rogue.strength();
        int strengthIncrease = 1;
        int expected = initialStrength + strengthIncrease;

        rogue.levelUp();
        int actual = rogue.strength();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedRogue_shouldIncreaseDexterityByFour() {
        Rogue rogue = new Rogue("Hades");
        int initialDexterity = rogue.dexterity();
        int dexterityIncrease = 4;
        int expected = initialDexterity + dexterityIncrease;

        rogue.levelUp();
        int actual = rogue.dexterity();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_initializedRogue_shouldIncreaseIntelligenceByOne() {
        Rogue rogue = new Rogue("Hades");
        int initialIntelligence = rogue.intelligence();
        int intelligenceIncrease = 1;
        int expected = initialIntelligence + intelligenceIncrease;

        rogue.levelUp();
        int actual = rogue.intelligence();

        assertEquals(expected, actual);
    }


    @Test
    public void sumTotalAttributes_noArmorEquipped_shouldReturnCorrectSum() {
        Rogue rogue = new Rogue("Hades");
        int heroStrength = 2;
        int heroDexterity = 6;
        int heroIntelligence = 1;

        // The following three variables are declared and initialized in order
        // to make calculations as explicit as possible
        int armorStrength = 0;
        int armorDexterity = 0;
        int armorIntelligence = 0;

        int sumOfRogueAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfArmorAttributes = armorStrength + armorDexterity + armorIntelligence;

        int expected = sumOfRogueAttributes + sumOfArmorAttributes;

        int actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_onePieceOfArmorEquipped_shouldReturnCorrectSum() throws InvalidArmorException {
        Rogue rogue = new Rogue("Hades");
        int heroStrength = 2;
        int heroDexterity = 6;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int sumOfRogueAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfRogueAttributes + sumOfArmorAttributes;

        rogue.equip(bodyArmor);
        int actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_twoPiecesOfEquipment_shouldReturnCorrectSum() throws InvalidArmorException {
        Rogue rogue = new Rogue("Hades");
        int heroStrength = 2;
        int heroDexterity = 6;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int headArmorStrength = 1;
        int headArmorDexterity = 1;
        int headArmorIntelligence = 1;

        int sumOfRogueAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;
        int sumOfHeadArmorAttributes = headArmorStrength + headArmorDexterity + headArmorIntelligence;

        int sumOfAllArmorAttributes = sumOfBodyArmorAttributes + sumOfHeadArmorAttributes;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor headArmor = new Armor(
                "Name",
                1,
                Item.Slot.HEAD,
                new HeroAttribute(headArmorStrength, headArmorDexterity, headArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfRogueAttributes + sumOfAllArmorAttributes;

        rogue.equip(bodyArmor);
        rogue.equip(headArmor);
        int actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void sumTotalAttributes_threePiecesOfEquipment_shouldReturnCorrectSum() throws InvalidArmorException {
        Rogue rogue = new Rogue("Hades");
        int heroStrength = 2;
        int heroDexterity = 6;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int headArmorStrength = 1;
        int headArmorDexterity = 1;
        int headArmorIntelligence = 1;

        int legsArmorStrength = 1;
        int legsArmorDexterity = 1;
        int legsArmorIntelligence = 1;

        int sumOfRogueAttributes = heroStrength + heroDexterity + heroIntelligence;

        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;
        int sumOfHeadArmorAttributes = headArmorStrength + headArmorDexterity + headArmorIntelligence;
        int sumOfLegsArmorAttributes = legsArmorStrength + legsArmorDexterity + legsArmorIntelligence;

        int sumOfAllArmorAttributes = sumOfBodyArmorAttributes + sumOfHeadArmorAttributes + sumOfLegsArmorAttributes;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor headArmor = new Armor(
                "Name",
                1,
                Item.Slot.HEAD,
                new HeroAttribute(headArmorStrength, headArmorDexterity, headArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor legsArmor = new Armor(
                "Name",
                1,
                Item.Slot.LEGS,
                new HeroAttribute(legsArmorStrength, legsArmorDexterity, legsArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfRogueAttributes + sumOfAllArmorAttributes;

        rogue.equip(bodyArmor);
        rogue.equip(headArmor);
        rogue.equip(legsArmor);
        int actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }


    @Test
    public void sumTotalAttributes_equipmentInGivenSpotReplaced_shouldReturnCorrectSum() throws InvalidArmorException {
        Rogue rogue = new Rogue("Hades");
        int heroStrength = 2;
        int heroDexterity = 6;
        int heroIntelligence = 1;

        int bodyArmorStrength = 1;
        int bodyArmorDexterity = 1;
        int bodyArmorIntelligence = 1;

        int altBodyArmorStrength = 1;
        int altBodyArmorDexterity = 1;
        int altBodyArmorIntelligence = 1;

        int sumOfRogueAttributes = heroStrength + heroDexterity + heroIntelligence;
        int sumOfBodyArmorAttributes = bodyArmorStrength + bodyArmorDexterity + bodyArmorIntelligence;

        Armor bodyArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(bodyArmorStrength, bodyArmorDexterity, bodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        Armor altArmor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(altBodyArmorStrength, altBodyArmorDexterity, altBodyArmorIntelligence),
                Armor.ArmorType.MAIL
        );

        int expected = sumOfRogueAttributes + sumOfBodyArmorAttributes;

        rogue.equip(bodyArmor);
        rogue.equip(altArmor);
        int actual = rogue.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_noWeaponEquipped_shouldReturnCorrectSum() {
        Rogue rogue = new Rogue("Hades");
        int damagingAttributes = rogue.dexterity(); // A rogue's damaging attribute is dexterity.
        int weaponDamage = 1; // Weapon damage is set to 1 when there is no weapon.
        int expected = weaponDamage * (1 + damagingAttributes / 100);

        int actual = rogue.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponEquipped_shouldReturnCorrectSum() throws InvalidWeaponException {
        Rogue rogue = new Rogue("Hades");
        int damagingAttributes = rogue.dexterity(); // A rogue's damaging attribute is dexterity.

        Weapon weapon = new Weapon("Name", SWORD, 1, 2);
        int weaponDamage = weapon.sumTotalAttributes();
        int expected = weaponDamage * (1 + damagingAttributes / 100);

        rogue.equip(weapon);
        int actual = rogue.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponEquippedAndReplaced_shouldReturnCorrectSum() throws InvalidWeaponException {
        Rogue rogue = new Rogue("Hades");
        int damagingAttributes = rogue.dexterity(); // A rogue's damaging attribute is dexterity.

        Weapon weapon = new Weapon("Name", SWORD, 1, 2);
        Weapon altWeapon = new Weapon("Name", SWORD, 1, 2);

        int altWeaponDamage = altWeapon.sumTotalAttributes();
        int expected = altWeaponDamage * (1 + damagingAttributes / 100);

        rogue.equip(weapon);
        rogue.equip(altWeapon);
        int actual = rogue.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damageCalculation_weaponAndArmorEquipped_shouldReturnCorrectSum() throws InvalidWeaponException, InvalidArmorException {
        Rogue rogue = new Rogue("Hades");
        int damagingAttributes = rogue.dexterity(); // A rogue's damaging attribute is dexterity.

        Weapon weapon = new Weapon("Name", SWORD, 1, 2);
        int weaponDamage = weapon.sumTotalAttributes();

        Armor armor = new Armor(
                "Name",
                1,
                Item.Slot.BODY,
                new HeroAttribute(1, 1, 1),
                Armor.ArmorType.MAIL
        );

        int expected = weaponDamage * (1 + damagingAttributes / 100);

        rogue.equip(armor);
        rogue.equip(weapon);
        int actual = rogue.damage();

        assertEquals(expected, actual);
    }
}