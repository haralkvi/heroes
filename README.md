This project was originally hosted on [a different repo](https://gitlab.com/haralkvi/heros). 
Check it out for full commit history.

# RPG Heroes
## Project Description
This repository contains Java modules written as an assignment in a Java backend development course.

The project currently contains the basis for a future, potential, very much hypothetical RPG (role-playing game).
This basis consists of two packages: one for heroes and one for items.

There are currently four different heroes: Mages, rangers, rogues and
warriors. These heroes have different qualities which make them _totally_ unique.

Items can either be weapons or armor. Items are equipped by heroes. Items, too, come in different varieties. Some items
may only be equipped by some heroes. Other items may only be equipped by other heroes.

## Future Plans
In the future, at some point in time, if possible, I'd like to implement console-based gameplay featuring heroes and items.
